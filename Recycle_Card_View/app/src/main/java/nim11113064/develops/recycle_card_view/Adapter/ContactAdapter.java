package nim11113064.develops.recycle_card_view.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import nim11113064.develops.recycle_card_view.Holder.ContactViewHolder;
import nim11113064.develops.recycle_card_view.Model.ContactPerson;
import nim11113064.develops.recycle_card_view.R;
//import nim11113064.develops.recycle_card_view.R;

/**
 * Created by Takiya on 20/06/2016.
 */
public class ContactAdapter extends RecyclerView.Adapter<ContactViewHolder> {

   ArrayList<ContactPerson> list;

    public  ContactAdapter (ArrayList<ContactPerson> li){
        list = li;

    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view,parent,false);

        return new ContactViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        ContactPerson  cp  = list.get(position);
        holder.vName.setText(cp.getNama());
        holder.vNomor.setText(cp.getNomor());
        holder.vEmail.setText(cp.getEmail());

    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
