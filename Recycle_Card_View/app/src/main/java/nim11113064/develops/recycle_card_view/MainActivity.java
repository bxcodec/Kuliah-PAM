package nim11113064.develops.recycle_card_view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import nim11113064.develops.recycle_card_view.Adapter.ContactAdapter;
import nim11113064.develops.recycle_card_view.Model.ContactPerson;

public class MainActivity extends AppCompatActivity {

    ArrayList<ContactPerson> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        data = new ArrayList<ContactPerson>();
        isiData();
        RecyclerView viewRec = (RecyclerView) findViewById(R.id.recView);
        viewRec.setHasFixedSize(true);
        LinearLayoutManager llm =  new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        viewRec.setLayoutManager(llm);
        ContactAdapter adapter = new ContactAdapter(data);

        viewRec.setAdapter(adapter);
    }
    void  isiData() {
        ContactPerson cp = new ContactPerson();
        cp.setNama("Iman");
        cp.setEmail("bxcodec@gmail.com");
        cp.setNomor("0822- 7408-5740");
        for(int i=0; i <10; i++) {
            data.add(cp);
        }

    }
}
