package nim11113064.develops.recycle_card_view.Model;

/**
 * Created by Takiya on 20/06/2016.
 */
public class ContactPerson {
    String nama;
    String nomor;
    String email;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
