package nim11113064.develops.recycle_card_view.Holder;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import nim11113064.develops.recycle_card_view.R;

/**
 * Created by Takiya on 20/06/2016.
 */
public class ContactViewHolder extends RecyclerView.ViewHolder {

    public TextView vName;
    public   TextView vNomor;
    public TextView vEmail;


    public ContactViewHolder(View v) {
        super(v);
        vName = (TextView) v.findViewById(R.id.txtName);
        vNomor = (TextView) v.findViewById(R.id.txtNomor);
        vEmail = (TextView) v.findViewById(R.id.txtEmail);

    }
}
