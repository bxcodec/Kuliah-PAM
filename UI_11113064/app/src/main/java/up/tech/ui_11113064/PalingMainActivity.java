package up.tech.ui_11113064;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class PalingMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paling_main);

        Button btnUI = (Button) findViewById(R.id.btnUI);
        Button btnDP = (Button) findViewById(R.id.btnDatePicker);
        Button btnSpiner = (Button) findViewById(R.id.btnSpinner);
        Button btnRadio = (Button) findViewById(R.id.btnRadio);
        Button [] btn = {btnUI, btnDP, btnSpiner, btnRadio};
        setListener(btn);
    }
    public  void  setListener(Button []  btn ) {
        for (Button button : btn ) {

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setActivity(v);
                }
            });
        }
    }

    public void setActivity(View v) {
        Intent intent;
        String tag = v.getTag().toString();
        switch (tag) {
            case "ui":
                intent = new Intent(this,MainActivity.class);
                break;
            case "dp":
                intent = new Intent(this,DatePickerActivity.class);
                break;
            case "sp":
                intent = new Intent(this,SpinnerActivity.class);
                break;
            case "rg":
                intent = new Intent(this,RadioBtnActivity.class);
                break;
            default:
                intent = new Intent(this,PalingMainActivity.class);
                break;
        }

        startActivity(intent);

    }

}
