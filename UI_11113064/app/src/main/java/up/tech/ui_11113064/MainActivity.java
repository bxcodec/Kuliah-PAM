package up.tech.ui_11113064;

import android.os.Bundle;
import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnHasil = (Button) findViewById(R.id.btnHasil);
        btnHasil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                klikHasil(v);

            }
        });
    }

    public void klikHasil(View v){
        TextView lblHasil = ( TextView) findViewById(R.id. lblHasil);
        CheckBox cbMK1=(CheckBox) findViewById(R.id. cbMK1);
        CheckBox cbMK2=(CheckBox) findViewById(R.id. cbMK2);
        CheckBox cbMK3=(CheckBox) findViewById(R.id. cbMK3);
        CheckBox cbMK4=(CheckBox) findViewById(R.id. cbMK4);
        CheckBox cbMK5=(CheckBox) findViewById(R.id. cbMK5);
        CheckBox cbMK6=(CheckBox) findViewById(R.id. cbMK6);
        CheckBox cbMK7=(CheckBox) findViewById(R.id. cbMK7);
        String str="";
        if(cbMK1.isChecked() ){
            str = str +"Bahasa Inggris VI " ;
        }
        if(cbMK2.isChecked()){
            str = str +"Tugas Akhir II " ;
        }
        if(cbMK3.isChecked()){
            str = str +"Kerja Praktek " ;
        }
        if(cbMK4.isChecked()){

            str = str +"Komputer dan Masyarakat ";
        }
        if(cbMK5.isChecked()){
            str = str +"Mobile Computing " ;
        }
        if(cbMK6.isChecked()){
            str = str +"Pancasila dan Kewarganegaraan " ;
        }
        if(cbMK7.isChecked()){
            str = str +"Topik Khusus " ;
        }
        lblHasil. setText(str);
    }
}
