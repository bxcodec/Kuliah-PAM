package up.tech.ui_11113064;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;

public class RadioBtnActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_radio_btn);
    }
    public void klikHasil(View v){
        TextView lblHasil = (TextView) findViewById(R.id. lblHasil);
        RadioGroup rgMK=(RadioGroup) findViewById(R.id. rgMK);
        int id= rgMK.getCheckedRadioButtonId();
        if(id==R.id.rbMK1){
            lblHasil.setText("Bahasa Inggris VI " );
        }
        if(id==R.id.rbMK2){
            lblHasil.setText("Tugas Akhir II " );
        }
        if(id==R.id.rbMK3){
            lblHasil.setText("Kerja Praktek " );
        }
        if(id==R.id.rbMK4){
            lblHasil.setText("Komputer dan Masyarakat " );
        }
        if(id==R.id.rbMK5){
            lblHasil.setText("Mobile Computing " );
        }
        if(id==R.id.rbMK6){
            lblHasil.setText("Pancasila dan Kewarganegaraan " );
        }
        if(id==R.id.rbMK7){
            lblHasil.setText("Topik Khusus " );
        }
    }
}

