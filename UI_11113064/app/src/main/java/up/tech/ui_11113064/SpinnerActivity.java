package up.tech.ui_11113064;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class SpinnerActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    String[] items ={"Bahasa Inggris VI" , "Tugas Akhir II" , "Kerja Praktek",
            "Komputer dan Masyarakat" , "Mobile Computing" , "Pancasila dan Kewarganegaraan", "Topik Khusus" };
    private TextView pilihan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);

        pilihan=(TextView) findViewById(R.id.lblPilih);
        Spinner spin=(Spinner) findViewById(R.id.spnPilih);
        spin.setOnItemSelectedListener( this);
        //Bagian yang menyebutkan desain tampilan yang akan digunakan
        //oleh Spinner dan data yang akan digunakan oleh Spinner.
        //Untuk kasus ini, data pilihan di-supply oleh array items
        ArrayAdapter<String> pil=new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, items);
        pil.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spin.setAdapter(pil);
    }
    public void onItemSelected(AdapterView<?> parent, View v, int
            posisi, long id){
        pilihan.setText( items[posisi]);
    }
    public void onNothingSelected(AdapterView<?> parent){
        pilihan.setText( "");
    }
}

