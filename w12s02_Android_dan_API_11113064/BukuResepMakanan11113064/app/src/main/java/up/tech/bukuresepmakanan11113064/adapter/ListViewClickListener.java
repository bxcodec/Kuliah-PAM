package up.tech.bukuresepmakanan11113064.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;

import java.util.ArrayList;

import up.tech.bukuresepmakanan11113064.Resep.ResepMinumanActivity;
import up.tech.bukuresepmakanan11113064.R;
import up.tech.bukuresepmakanan11113064.Resep.ResepAyamActivity;
import up.tech.bukuresepmakanan11113064.Resep.ResepKhasActivity;
import up.tech.bukuresepmakanan11113064.Resep.ResepKueActivity;
import up.tech.bukuresepmakanan11113064.Resep.ResepPraktisActivity;
import up.tech.bukuresepmakanan11113064.Resep.ResepSambalActivity;
import up.tech.bukuresepmakanan11113064.Resep.ResepUmumActivity;

/**
 * Created by Takiya on 09/04/2016.
 */
public class ListViewClickListener implements AdapterView.OnItemClickListener{
    ArrayList<PacketData> listItem;
    Context mcontex;

    public ListViewClickListener(Context context, ArrayList<PacketData> data) {
        listItem = data;
        mcontex = context;
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//        Log.i("ONCLICK MAMMEN", "MASUK MAMMMEN");
        changeActivity(listItem.get(position).getIdData());
    }

    void changeActivity(int id){
        Intent intent = null;
        switch (id){
            case R.drawable.umum:
                intent = new Intent(mcontex, ResepUmumActivity.class);
                break;
            case R.drawable.ayam:
                intent = new Intent(mcontex, ResepAyamActivity.class);
                break;
            case R.drawable.minuman:
                intent = new Intent(mcontex, ResepMinumanActivity.class);
                break;
            case R.drawable.kue:
                intent = new Intent(mcontex, ResepKueActivity.class);
                break;
            case R.drawable.khas:
                intent = new Intent(mcontex, ResepKhasActivity.class);
                break;
            case R.drawable.praktis:
                intent = new Intent(mcontex, ResepPraktisActivity.class);
                break;
            case R.drawable.sambal:
                intent = new Intent(mcontex, ResepSambalActivity.class);
                break;

        }
        mcontex.startActivity(intent);
    }


}
