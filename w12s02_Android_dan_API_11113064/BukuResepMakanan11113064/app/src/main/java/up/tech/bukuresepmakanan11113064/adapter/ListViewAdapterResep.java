package up.tech.bukuresepmakanan11113064.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import up.tech.bukuresepmakanan11113064.R;

/**
 * Created by Takiya on 09/04/2016.
 */
public class ListViewAdapterResep extends BaseAdapter {
    ArrayList<PacketData> data;
    Context mcontext;
    public ListViewAdapterResep(ArrayList<PacketData> data, Context context){
        this.data = data;
        this.mcontext = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    class  viewHolder {
        public ImageView imageView;
        public TextView text;


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        viewHolder holder ;
        LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        if(convertView==null){
            convertView = inflater.inflate(R.layout.list_menuresep_item,null);
            holder = new viewHolder();
            holder.imageView = (ImageView) convertView.findViewById(R.id.imageListMenuResepItem);
            holder.text= (TextView) convertView.findViewById(R.id.lblListMenuResepItem);
            convertView.setTag(holder);

        }
        else {
            holder = (viewHolder) convertView.getTag();
        }

        holder.imageView.setImageDrawable(mcontext.getResources().getDrawable(data.get(position).getIdData()));
        holder.imageView.setScaleType(ImageView.ScaleType.FIT_XY);

        holder.text.setText(data.get(position).getLbl());


        return convertView;
    }


}
