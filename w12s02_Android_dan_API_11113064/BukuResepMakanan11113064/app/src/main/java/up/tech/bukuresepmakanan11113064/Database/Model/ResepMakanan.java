package up.tech.bukuresepmakanan11113064.Database.Model;

/**
 * Created by Takiya on 30/04/2016.
 */
public class ResepMakanan {
    int id_resep;
    String Nama;
    String Deskripsi;
    String Bahan;
    String Cara_Membuat;

    public int getId_resep() {
        return id_resep;
    }

    public void setId_resep(int id_resep) {
        this.id_resep = id_resep;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String nama) {
        Nama = nama;
    }

    public String getDeskripsi() {
        return Deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        Deskripsi = deskripsi;
    }

    public String getBahan() {
        return Bahan;
    }

    public void setBahan(String bahan) {
        Bahan = bahan;
    }

    public String getCara_Membuat() {
        return Cara_Membuat;
    }

    public void setCara_Membuat(String cara_Membuat) {
        Cara_Membuat = cara_Membuat;
    }
}
