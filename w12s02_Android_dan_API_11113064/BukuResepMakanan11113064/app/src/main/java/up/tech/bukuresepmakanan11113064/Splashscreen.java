package up.tech.bukuresepmakanan11113064;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import up.tech.bukuresepmakanan11113064.API.POJOResep;
import up.tech.bukuresepmakanan11113064.API.RESTClient;
import up.tech.bukuresepmakanan11113064.Database.Controller.ResepMakananController;
import up.tech.bukuresepmakanan11113064.Database.Model.ResepMakanan;

public class Splashscreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        final int idcategory = 1;
        RESTClient.get().getResepByCategory(idcategory, new Callback<POJOResep>() {
            @Override
            public void success(POJOResep pojoResep, Response response) {
                if(pojoResep.getResepbycategory().size()>0){
                    ResepMakananController rsp = new ResepMakananController(getApplicationContext());
                    try {
                        rsp.open();
                        for(int i=0; i<pojoResep.getResepbycategory().size();i++){
                            ResepMakanan mkn = pojoResep.getResepbycategory().get(i);
                            rsp.insertData(mkn.getId_resep(),mkn.getNama(),mkn.getDeskripsi(),mkn.getBahan(),mkn.getCara_Membuat());

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    finally {
                        rsp.close();
                    }
//                    rsp.insertData();
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });




        fade();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                 final Intent mainIntent =  new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(mainIntent);
                //blink();

                finish();
            }
        }, 3500);



    }
    public void blink() {
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.blink);
        imageView.startAnimation(animation);

    }
    public void slide() {
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.slide);
        imageView.startAnimation(animation);

    }
    public void fade() {
        ImageView imageView = (ImageView) findViewById(R.id.imageView);
        Animation animation = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade);
        imageView.startAnimation(animation);

    }
}
