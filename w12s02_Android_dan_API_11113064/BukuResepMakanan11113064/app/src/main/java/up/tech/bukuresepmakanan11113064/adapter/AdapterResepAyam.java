package up.tech.bukuresepmakanan11113064.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import up.tech.bukuresepmakanan11113064.Database.Model.ResepMakanan;
import up.tech.bukuresepmakanan11113064.R;

/**
 * Created by Takiya on 30/04/2016.
 */
public class AdapterResepAyam extends BaseAdapter{

        ArrayList<ResepMakanan> data;
        Context mcontext;
        public AdapterResepAyam(ArrayList<ResepMakanan> data, Context context){
            this.data = data;
            this.mcontext = context;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        class  viewHolder {
            public TextView Nama;
            public TextView Deskripsi;
            public TextView Bahan;
            public TextView Cara;


        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            viewHolder holder ;
            LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


            if(convertView==null){
                convertView = inflater.inflate(R.layout.list_menuresep_item,null);
                holder = new viewHolder();
                holder.Nama = (TextView) convertView.findViewById(R.id.namaResep);
                holder.Deskripsi= (TextView) convertView.findViewById(R.id.deskripsiResep);
                holder.Bahan= (TextView) convertView.findViewById(R.id.bahanResep);
                holder.Cara= (TextView) convertView.findViewById(R.id.caraMembuat);
                convertView.setTag(holder);

            }
            else {
                holder = (viewHolder) convertView.getTag();
            }

            holder.Nama.setText(data.get(position).getNama());
            holder.Deskripsi.setText(data.get(position).getDeskripsi());
            holder.Bahan.setText(data.get(position).getBahan());
            holder.Cara.setText(data.get(position).getCara_Membuat());
            return convertView;
        }


    }


