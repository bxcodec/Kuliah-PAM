package up.tech.bukuresepmakanan11113064.Resep;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import up.tech.bukuresepmakanan11113064.R;

public class ResepKhasActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resep_khas);
        TextView credit = (TextView) findViewById(R.id.credits);
        credit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bukaBrowser("http://bxcode-rev.blogpsot.com");
            }
        });

        TextView Web = (TextView) findViewById(R.id.webSite);
        Web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bukaBrowser("http://google.com");
            }
        });
    }
    private void bukaBrowser (String alamat) {

        Uri uri =  Uri.parse(alamat);
        Intent i = new Intent(Intent.ACTION_VIEW,uri);
        startActivity(i);
    }

}
