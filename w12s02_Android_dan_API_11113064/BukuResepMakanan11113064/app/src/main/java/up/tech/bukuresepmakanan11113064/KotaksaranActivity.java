package up.tech.bukuresepmakanan11113064;

import android.app.Dialog;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

import up.tech.bukuresepmakanan11113064.Database.Controller.SaranController;
import up.tech.bukuresepmakanan11113064.Database.Model.Saran;
import up.tech.bukuresepmakanan11113064.adapter.ListViewAdapterSaran;

public class KotaksaranActivity extends AppCompatActivity {

    ArrayList<Saran>sarans;
    SaranController controller;
    EditText topik ;
    EditText isiSaran;
    Button simpanSaran;
    ListViewAdapterSaran adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kotaksaran);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        controller = new SaranController(this);
        sarans = new ArrayList<>();
        getSarans();
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setDialog();
            }
        });

        ListView listView = (ListView) findViewById(R.id.listViewSaran);
        adapter = new ListViewAdapterSaran(this,sarans);
        listView.setAdapter(adapter);

    }


    private void getSarans(){

        try{
            controller.open();
            sarans = controller.getData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setDialog() {

        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.form_saran);
        dialog.setTitle("Add New Saran");


        topik = (EditText) dialog.findViewById(R.id.judulSaran);
        isiSaran = (EditText) dialog.findViewById(R.id.isiSaran);
        simpanSaran = (Button) dialog.findViewById(R.id.simpanSaranButton);
            simpanSaran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try{
                    controller.open();
                    controller.insertData((int) System.currentTimeMillis(), String.valueOf(topik.getText()), String.valueOf(isiSaran.getText()));
                    dialog.dismiss();
                    sarans.clear();
                    sarans = controller.getData();
                    adapter.setSarans(sarans);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dialog.show();
    }

}
