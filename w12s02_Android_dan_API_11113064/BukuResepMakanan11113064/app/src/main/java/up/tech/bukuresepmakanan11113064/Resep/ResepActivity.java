package up.tech.bukuresepmakanan11113064.Resep;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import up.tech.bukuresepmakanan11113064.R;
import up.tech.bukuresepmakanan11113064.adapter.PacketData;

public class ResepActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resep);

        PacketData data = (PacketData) getIntent().getSerializableExtra("dataToView");

        TextView textView = (TextView) findViewById(R.id.lblAfterClicked);
        textView.setText(data.getLbl());
        ImageView imageView = (ImageView) findViewById(R.id.imageAfterClicked);
        imageView.setImageResource(data.getIdData());

    }
}
