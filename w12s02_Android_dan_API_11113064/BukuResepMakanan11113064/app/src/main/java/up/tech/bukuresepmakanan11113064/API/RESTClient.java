package up.tech.bukuresepmakanan11113064.API;

import retrofit.RestAdapter;

/**
 * Created by Takiya on 30/04/2016.
 */
public class RESTClient {
    private static ResepMakananAPI REST_CLIENT;
    private static String BASE_URL = "http://172.35.40.64/resepmakanan";

    static {
        setupRestClient();
    }

    private RESTClient() {

    }

    public static ResepMakananAPI get() {
        return REST_CLIENT;
    }

    private static void setupRestClient() {
        RestAdapter builder = new RestAdapter.Builder().setEndpoint(BASE_URL).build();
        REST_CLIENT = builder.create(ResepMakananAPI.class);
    }

}

