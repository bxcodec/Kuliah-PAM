package up.tech.bukuresepmakanan11113064.API;

import retrofit.Callback;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by Takiya on 30/04/2016.
 */
public interface ResepMakananAPI {

    @POST("/resepbycategory/{category}")
    void getResepByCategory(@Path("category") int id_category , Callback<POJOResep> callback );
}
