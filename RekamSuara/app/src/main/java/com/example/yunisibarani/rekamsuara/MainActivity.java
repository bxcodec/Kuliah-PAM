package com.example.yunisibarani.rekamsuara;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;

public class MainActivity extends Activity {
    private Button btnrekam;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btnrekam = (Button)findViewById(R.id.button3);
        btnrekam.setOnClickListener(new AdapterView.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), RekamActivity.class);
                startActivity(intent);
            }
        });
    }

}

