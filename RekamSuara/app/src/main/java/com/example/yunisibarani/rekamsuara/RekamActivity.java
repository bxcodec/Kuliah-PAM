package com.example.yunisibarani.rekamsuara;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.AdapterView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class RekamActivity extends AppCompatActivity {
    private Button rekam, stopr, putar, stopp;
    private TextView txt;
    private MediaPlayer mp;
    private MediaRecorder mr;
    private String tempat;
    public static File myfile;
    private Context mcontext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rekam);

        tempat = Environment.getExternalStorageDirectory() + "/hasilRekam.wav";
        txt = (TextView)findViewById(R.id.textView1);
        rekam = (Button)findViewById(R.id.button1);
        rekam.setOnClickListener(new AdapterView.OnClickListener() {
            public void onClick(View v) {
//                startRekam();
                txt.setText("Recording...");
                rekam.setEnabled(false);
                stopr.setEnabled(true);
                putar.setEnabled(false);
                stopp.setEnabled(false);
            }
        });
        stopr = (Button)findViewById(R.id.button2);
        stopr.setEnabled(false);
        stopr.setOnClickListener(new AdapterView.OnClickListener() {
            public void onClick(View v) {
//                stopRekam();
                txt.setText("");
                rekam.setEnabled(true);
                stopr.setEnabled(false);
                putar.setEnabled(true);
                stopp.setEnabled(false);
            }
        });
        putar = (Button)findViewById(R.id.button3);
        putar.setOnClickListener(new AdapterView.OnClickListener() {
            public void onClick(View v) {
//                startHasil();
                txt.setText("Playing...");
                rekam.setEnabled(false);
                stopr.setEnabled(false);
                putar.setEnabled(false);
                stopp.setEnabled(true);
            }
        });
        stopp = (Button)findViewById(R.id.button4);
        stopp.setEnabled(false);
        stopp.setOnClickListener(new AdapterView.OnClickListener() {
            public void onClick(View v) {
//                stopHasil();
                txt.setText("");
                rekam.setEnabled(true);
                stopr.setEnabled(false);
                putar.setEnabled(true);
                stopp.setEnabled(false);
            }
        });
    }


    public File GetFileTOwriteSound()
    {
        File tempPicFile=null;
        String ext_storage_state= Environment.getExternalStorageState();
        File mediaStorage=new File(Environment.getExternalStorageDirectory()+"/TAUKY/SOUNDS");
        if(ext_storage_state.equalsIgnoreCase(Environment.MEDIA_MOUNTED))
        {
            if(!mediaStorage.exists())
            {
                mediaStorage.mkdirs();
            }else
            {
                //do nothing
            }

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            tempPicFile = new File(mediaStorage.getPath() + File.separator
                    + "SOUND_" + timeStamp + ".wav");

//            myfile=tempPicFile;

        }
        else
        {
            Toast.makeText(mcontext, "NO SDCARD MOUNTED", Toast.LENGTH_LONG).show();
        }
        return tempPicFile;
    }

    public void startRekam(){
        if(mr != null){
            mr.release();
        }
        File fout = new File(tempat);
        if(fout != null){
            fout.delete();
        }
        mr = new MediaRecorder();
        mr.setAudioSource(MediaRecorder.AudioSource.MIC);
        mr.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mr.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        mr.setOutputFile(tempat);
        try {
            mr.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mr.start();
    }
    public void stopRekam(){
        if(mr != null){
            mr.stop();
            mr.release();
            mr = null;
        }
    }
    public void startHasil(){
        mp = new MediaPlayer();
        try {
            mp.setDataSource(tempat);
            mp.prepare();
            mp.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void stopHasil(){
        if(mp != null){
            if(mp.isPlaying())
                mp.stop();
            mp.release();
            mp = null;
        }
    }
}
