package up.tech.recordingwav;

import android.util.Log;

/**
 * Created by Takiya on 30/03/2016.
 */
public class AppLog {
    private static  final String APP_TAG = "AUDIO RECORDER";
    public  static  int  logString(String Message){

        return Log.i(APP_TAG,Message);

    }
}
