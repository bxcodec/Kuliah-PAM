package up.tech.resource_11113064;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnRelative = (Button) findViewById(R.id.btnRelative);
        btnRelative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeActivity(v);
            }
        });



        Button btnLinear = (Button) findViewById(R.id.btnLinear);
        btnLinear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            changeActivity(v);
            }
        });


        Button btnTable= (Button) findViewById(R.id.btnTableLayout);
        btnTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeActivity(v);
            }
        });


    }

    public  void  changeActivity(View v) {
        Intent intent=null;
        String Btn = v.getTag().toString();
        switch (Btn) {
            case "relative":

                intent = new Intent(this,RelativeLayoutActivity.class);
                startActivity(intent);
                break;
            case "linear" :

                intent = new Intent(this,LinearLayoutActivity.class);
                startActivity(intent);
                break;
            case "table":

                intent = new Intent(this,TableLayoutActivity.class);
                startActivity(intent);
                break;
            default:
                intent = new Intent(this,MainActivity.class);
                startActivity(intent);
                break;


        }

    }

}
