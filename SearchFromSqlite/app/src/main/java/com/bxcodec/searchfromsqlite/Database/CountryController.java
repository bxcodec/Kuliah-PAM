package com.bxcodec.searchfromsqlite.Database;

import android.app.SearchManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Takiya on 17/05/2016.
 */
public class CountryController  {

    DBController dbHelper;
    SQLiteDatabase database;
    private static final String FIELD_ID = "_id";
    private static final String FIELD_NAME = "_name";
    private static final String FIELD_FLAG = "_flag";
    private static final String FIELD_CURRENCY = "_currency";
    private static final String TABLE_NAME = "countries";
    private HashMap<String, String> mAliasMap;

    public  static  String CREATE_TABEL_CATEGORY= "create table "
            +TABLE_NAME + " (" + FIELD_ID + " integer primary key, " + FIELD_NAME+ " text, " + FIELD_FLAG + " integer, "
            + FIELD_CURRENCY+" text)";


    public  String [] tblColoumn = {FIELD_ID, FIELD_NAME , FIELD_FLAG, FIELD_CURRENCY};




    public CountryController(Context c) {
        Log.i("query" ,""+CREATE_TABEL_CATEGORY);
        dbHelper = new DBController( c);
        // This HashMap is used to map table fields to Custom Suggestion fields
        mAliasMap = new HashMap<String, String>();

        // Unique id for the each Suggestions ( Mandatory )
        mAliasMap.put("_ID", FIELD_ID + " as " + "_id" );

        // Text for Suggestions ( Mandatory )
        mAliasMap.put(SearchManager.SUGGEST_COLUMN_TEXT_1, FIELD_NAME + " as " + SearchManager.SUGGEST_COLUMN_TEXT_1);

// Icon for Suggestions ( Optional )
        mAliasMap.put( SearchManager.SUGGEST_COLUMN_ICON_1, FIELD_FLAG + " as " + SearchManager.SUGGEST_COLUMN_ICON_1);

        // This value will be appended to the Intent data on selecting an item from Search result or Suggestions ( Optional )
        mAliasMap.put( SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID, FIELD_ID + " as " + SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID );




    }

    public Cursor getCountries(String[] selectionArgs){

        String selection = FIELD_NAME + " like ? ";

        if(selectionArgs!=null){
            selectionArgs[0] = "%"+selectionArgs[0] + "%";
        }

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        queryBuilder.setProjectionMap(mAliasMap);

        queryBuilder.setTables(TABLE_NAME);

        Cursor c = queryBuilder.query(dbHelper.getReadableDatabase(),
                new String[] { "_ID",
                        SearchManager.SUGGEST_COLUMN_TEXT_1 ,
                        SearchManager.SUGGEST_COLUMN_ICON_1 ,
                        SearchManager.SUGGEST_COLUMN_INTENT_DATA_ID } ,
                selection,
                selectionArgs,
                null,
                null,
                FIELD_NAME + " asc ","10"
        );
        return c;
    }

    /** Return Country corresponding to the id */
    public Cursor getCountry(String id){

        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        queryBuilder.setTables(TABLE_NAME);

        Cursor c = queryBuilder.query(dbHelper.getReadableDatabase(),
                new String[] { "_id", "_name", "_flag", "_currency" } ,
                "_id = ?", new String[] { id } , null, null, null ,"1"
        );

        return c;
    }

    public void  open() throws  Exception{

        database= dbHelper.getWritableDatabase();

    }

    public  void close() {
        dbHelper.close();
    }

    public void deleteData() {
        database.delete(TABLE_NAME, null, null);
    }

    public  void insertData( String nama , int  flag, String currency) {

        ContentValues contentValues =  new ContentValues();
        contentValues.put(FIELD_ID,flag);
        contentValues.put(FIELD_NAME,nama);
        contentValues.put(FIELD_FLAG,flag);
        contentValues.put(FIELD_CURRENCY,currency);
        database.insert(TABLE_NAME, null, contentValues);

    }


    public ArrayList<CountryModel> getCountryByName(String nama) {

        ArrayList<CountryModel> allData = new ArrayList<CountryModel>();
        CountryModel model = new CountryModel();
        Cursor cursor = null;
        cursor = database.query(TABLE_NAME,tblColoumn, FIELD_NAME +"LIKE ?",new String[]{nama},null,null,null);


//        Cursor cursor = null;
//        cursor = database.query(TABLE_NAME, tblColoumn, null, null, null,null, FIELD_ID+ " ASC");
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            allData.add(parseData(cursor));
            cursor.moveToNext();
        }

        cursor.close();
        return allData;

    }

    public ArrayList<CountryModel> getData() {


        ArrayList<CountryModel> allData = new ArrayList<CountryModel>();
        Cursor cursor = null;
        cursor = database.query(TABLE_NAME, tblColoumn, null, null, null,null, FIELD_ID+ " ASC");
        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            allData.add(parseData(cursor));
            cursor.moveToNext();
        }

        cursor.close();
        return allData;

    }


    private CountryModel parseData(Cursor cursor) {
        CountryModel curData = new CountryModel();
        curData.setId(cursor.getInt(0));
        curData.setNama(cursor.getString(1));
            curData.setFlag(cursor.getInt(2));
            curData.setCurrency(cursor.getString(3));
//            curData.setCara_Membuat(cursor.getString(4));
        return curData;
    }






}
