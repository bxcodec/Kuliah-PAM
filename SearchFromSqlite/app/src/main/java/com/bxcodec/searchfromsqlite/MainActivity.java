package com.bxcodec.searchfromsqlite;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.bxcodec.searchfromsqlite.Database.CountryController;
import com.bxcodec.searchfromsqlite.Database.CountryModel;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    CountryController dbConttroller ;
    ArrayList<CountryModel> models;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbConttroller = new CountryController(this);

        isiData();

        Button btn = (Button) findViewById(R.id.btnSearch);
//        Sear
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onSearchRequested();
//                Intent intent = new Intent(v.getContext(), SearchActivity.class);
//                startActivity(intent);
            }
        });

    }
    void isiData() {

        try {
            dbConttroller.open();
            dbConttroller.deleteData();

             for(int i =0 ; i <Country.countries.length ; i++ ){
                 dbConttroller.insertData(Country.countries[i],Country.flags[i],Country.currency[i]);
                }
        } catch (Exception e) {
            e.printStackTrace();
        }

        dbConttroller.close();
    }
}
