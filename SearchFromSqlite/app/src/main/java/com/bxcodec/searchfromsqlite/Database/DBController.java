package com.bxcodec.searchfromsqlite.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Takiya on 17/05/2016.
 */
public class DBController  extends SQLiteOpenHelper{

    public final static  String DBNAME = "country.db";
    private static int DBVersion =1;


    public DBController(Context context) {
        super(context, DBNAME, null, DBVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        try {
            db.execSQL(CountryController.CREATE_TABEL_CATEGORY);
        }
        catch (Exception e)
        {
            Log.e("ERORR", "DATABASE" , e);
        }



//        db.
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
