package com.bxcodec.searchfromsqlite;

import android.app.SearchManager;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.bxcodec.searchfromsqlite.Database.CountryController;

/**
 * Created by Takiya on 17/05/2016.
 */
public class CountryContentProvider extends ContentProvider {
    CountryController dbControler;

    public static final String AUTHORITY = "com.bxcodec.searchfromsqlite.CountryContentProvider";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/countries" );


    private static final int SUGGESTIONS_COUNTRY = 1;
    private static final int SEARCH_COUNTRY = 2;
    private static final int GET_COUNTRY = 3;
    UriMatcher  uriMatcher = buildUriMatcher();
    private  UriMatcher buildUriMatcher () {
        UriMatcher  uriMatcher  =  new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI(AUTHORITY, SearchManager.SUGGEST_URI_PATH_QUERY,SUGGESTIONS_COUNTRY);
        uriMatcher.addURI(AUTHORITY , "countries" , SEARCH_COUNTRY);
        uriMatcher.addURI(AUTHORITY,"countries/#",GET_COUNTRY);
        return  uriMatcher;

    }

    @Override
    public boolean onCreate() {
        dbControler = new CountryController(getContext());
        return false;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        Cursor c = null;
        switch (uriMatcher.match(uri)  ) {
            case  SUGGESTIONS_COUNTRY:
                c =dbControler.getCountries(selectionArgs);
            break;
            case SEARCH_COUNTRY :
                c =  dbControler.getCountries(selectionArgs);
                break;
            case GET_COUNTRY:
                String id = uri.getLastPathSegment();

                c = dbControler.getCountry(id);
                break;

        }
        return c;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }
}
