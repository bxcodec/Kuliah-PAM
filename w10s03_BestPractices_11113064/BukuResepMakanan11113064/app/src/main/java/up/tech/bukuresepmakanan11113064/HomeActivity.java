package up.tech.bukuresepmakanan11113064;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
//import android.support.v4.app.ActivityCompat21;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class HomeActivity extends AppCompatActivity {

    private SharedPreferences pref;
    private SharedPreferences.Editor editor;
    private final String KEY_NAME = "user";



    public static class DialogFragent extends DialogFragment {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            AlertDialog.Builder builder =  new AlertDialog.Builder(getActivity());
            builder.setMessage(R.string.exitPopup).setPositiveButton(R.string.exitYes, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
//                    if (Build.VERSION.SDK_INT >= 21) {
//                        ActivityCompat21.finishAfterTransition(activity);
//                    } else {
                        getActivity().finish();
//                    }
                }
            }).setNegativeButton(R.string.exitNo, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
//                    Toast.makeText(getActivity().getApplicationContext(), "Yuhuu",Toast.LENGTH_LONG).show();
//                    getActivity().
                }

            });


            return builder.create();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
        pref = getSharedPreferences("mypreferences", MODE_PRIVATE);
        String savedName = pref.getString(KEY_NAME, "-") ;


        TextView loggedUser = (TextView) findViewById(R.id.preferencesWellcome);

        String user ="Wellcome " + pref.getString(KEY_NAME,"-");
        loggedUser.setText(user);


//        loggedUser.setT
        Button btnGo = (Button) findViewById(R.id.lihatMenu);
        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                changeActivity(v);
            }
        });

        TextView credit = (TextView) findViewById(R.id.credits);
        credit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bukaBrowser("http://bxcode-rev.blogpsot.com");
            }
        });

        TextView Web = (TextView) findViewById(R.id.webSite);
        Web.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                bukaBrowser("http://google.com");
            }
        });


        //jalankan service

        final Intent intent =  new Intent(HomeActivity.this, ServiceResepMakanan.class);
        startService(intent);


        Button btnStopService = (Button) findViewById(R.id.resepService);
        btnStopService.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                stopService(intent);

            }
        });
        Button btnInvite = (Button) findViewById(R.id.inviteTeman);
        btnInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            inviteFriendActivity(v);

            }
        });


        batteryLevel = (TextView) findViewById(R.id. batteryLevel) ;
        mBatteryLevelProgress = (ProgressBar) findViewById(R.id.batteryProgress) ;
        this.registerReceiver(this.myBatteryReceiver, new
                IntentFilter(Intent.ACTION_BATTERY_CHANGED)) ;
    }



    public  void saran(View v){

        Intent inten =new Intent(this,KotaksaranActivity.class);
        startActivity(inten);
    }

    private void bukaBrowser (String alamat) {

        Uri uri =  Uri.parse(alamat);
        Intent i = new Intent(Intent.ACTION_VIEW,uri);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.about) {
            Intent intent = new Intent(this,AboutUsActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }

    public  void changeActivity(View view) {

        Intent intent = new Intent(this,MenuresepActivity.class);
        startActivity(intent);
    }
    public  void inviteFriendActivity(View view) {
        Toast.makeText(this, "OKEEE" , Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this,InviteFriend.class);
        startActivity(intent);

    }



    private TextView batteryLevel;
    private ProgressBar mBatteryLevelProgress;

    private BroadcastReceiver myBatteryReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int lvl = intent.getIntExtra("level", 0) ;
            batteryLevel.setText("Battery anda saat ini : " +String. valueOf(lvl) + "%") ;
            mBatteryLevelProgress.setProgress(lvl) ;
            if (lvl == 100) {
//                Toast. makeText(context, "Battery Full.",Toast. LENGTH_LONG).show() ;
                notif("Baterai Anda Sudah Penuh","Selamat");
            }

            if(lvl<5) {
                notif("Baterai Anda Lemah","Perhatian");
//                Toast. makeText(context, "Battery LOW.",Toast. LENGTH_LONG).show() ;
            }
        }
    } ;


    @Override
    public void onBackPressed() {

        DialogFragent fragent = new DialogFragent();
        fragent.show(getFragmentManager(), "exit");

    //    Log.i("YEE", "BACKK");

//        super.onBackPressed();

    }



    public void notif(String message, String TAG) {
        //Setting notification yang ingin ditampilkan
        NotificationCompat.Builder mBuilder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.notif)
                        .setContentTitle(TAG)
                        .setContentText(message)
                        .setAutoCancel(true) //menghilangkan notifikasi
        //ketika notifikasi di tekan
        .setVibrate(new long[]{ 1000, 1000, 1000, 1000,
                1000});
        // Membuat intent yang dituju ketika notifikasi ditekan
        Intent resultIntent = new Intent(this, MenuresepActivity. class);
        // objek stack builder digunakan untuk memastikan ketika back
        //ditekan dari, aplikasi anda akan menuju homescreen
        TaskStackBuilder stackBuilder = TaskStackBuilder. create(this);
        stackBuilder.addParentStack(MenuresepActivity. class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent. FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager)
                        getSystemService(Context. NOTIFICATION_SERVICE);
        // 101 adalah ID notifikasi, digunakan jika kita ingin mengupdate notifikasi.
        mNotificationManager.notify(101, mBuilder.build());
    }

}

