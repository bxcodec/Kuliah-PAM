package up.tech.bukuresepmakanan11113064.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
//import android.widget.BaseAdapter;

import java.util.ArrayList;

import up.tech.bukuresepmakanan11113064.Database.Model.Saran;
import up.tech.bukuresepmakanan11113064.R;

/**
 * Created by Takiya on 12/04/2016.
 */
public class ListViewAdapterSaran extends ArrayAdapter<Saran> {

    Context mcontex;
    ArrayList<Saran> sarans;

    static  class  ViewHolder  {
        TextView topik;
        TextView isiSaran;

    }

    public ListViewAdapterSaran(Context mcontex, ArrayList<Saran> sarans) {
        super(mcontex, R.layout.saran_list_item,sarans);
        this.mcontex = mcontex;
        this.sarans = sarans;

    }



    public void setSarans(ArrayList<Saran> sarans) {
        this.sarans.clear();

        for (Saran saran: sarans) {
            this.sarans.add(saran);
        }

        notifyDataSetChanged();

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder ;
        Saran saran = getItem(position);
        if(convertView==null){
            holder = new ViewHolder();
            LayoutInflater inflater =  LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.saran_list_item,parent , false);
            holder.topik = (TextView) convertView.findViewById(R.id.viewTopikSaran);
            holder.isiSaran= (TextView) convertView.findViewById(R.id.viewIsiSaran);
            convertView.setTag(holder);
        }
        else {
            holder = (ViewHolder) convertView.getTag();

        }

        holder.isiSaran.setText(saran.getSaran());
        holder.topik.setText(saran.getJudul());

        return convertView;
    }
}
