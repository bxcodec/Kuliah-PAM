package up.tech.bukuresepmakanan11113064.Database.Controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import up.tech.bukuresepmakanan11113064.Database.Model.Saran;

/**
 * Created by Takiya on 12/04/2016.
 */
public class SaranController {


        DbController dbHelper;
        SQLiteDatabase database;
        public static final String TABLE_NAME = "saran";
        public static final String ID = "id";
        public static final String USER = "username";
        public static final String SARAN = "saran";

        public  static  String CREATE_TABEL_SARAN= "create table "
                +TABLE_NAME + " (" + ID+ " integer primary key, "+USER + " text,"
                +SARAN + " text)";

        public  String [] tblColoumn = {ID,USER,SARAN};

        public SaranController (Context c) {
            dbHelper = new DbController(c);

        }
        public void  open() throws  Exception{

            database= dbHelper.getWritableDatabase();

        }

        public  void close() {
            dbHelper.close();
        }

        public void deleteData() {
            database.delete(TABLE_NAME, null, null);
        }

        public  void insertData( int id , String user, String saran) {
            ContentValues contentValues =  new ContentValues();
            contentValues.put(ID,id);
            contentValues.put(USER,user);
            contentValues.put(SARAN,saran);
            database.insert(TABLE_NAME, null, contentValues);

        }

        public ArrayList<Saran> getData() {


            ArrayList<Saran> allData = new ArrayList<Saran>();
            Cursor cursor = null;
            cursor = database.query(TABLE_NAME, tblColoumn, null, null, null,
                    null, ID + " ASC");
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                allData.add(parseData(cursor));
                cursor.moveToNext();
            }
            cursor.close();
            return allData;

        }


    private Saran parseData(Cursor cursor) {
        Saran curData = new Saran();
        curData.setId(cursor.getInt(0));
        curData.setJudul(cursor.getString(1));
        curData.setSaran(cursor.getString(2));
        return curData;
    }

    }

