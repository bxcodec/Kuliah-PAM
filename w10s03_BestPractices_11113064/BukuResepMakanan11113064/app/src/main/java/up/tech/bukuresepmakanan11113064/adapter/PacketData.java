package up.tech.bukuresepmakanan11113064.adapter;

import java.io.Serializable;

/**
 * Created by Takiya on 18/03/2016.
 */
public class PacketData implements Serializable{
    private String lbl;
    private int idData;

    @Override
    public String toString() {
        return "PacketData{" +
                "lbl='" + lbl + '\'' +
                ", idData=" + idData +
                '}';
    }

    public int getIdData() {
        return idData;
    }

    public void setIdData(int idData) {
        this.idData = idData;
    }

    public String getLbl() {
        return lbl;
    }

    public void setLbl(String lbl) {
        this.lbl = lbl;
    }

    public PacketData(String lbl, int idData) {
        this.lbl = lbl;
        this.idData = idData;
    }


    public PacketData() {
    }
    public PacketData(PacketData data) {
        this.lbl = data.getLbl();
        this.idData = data.getIdData();
    }
}
