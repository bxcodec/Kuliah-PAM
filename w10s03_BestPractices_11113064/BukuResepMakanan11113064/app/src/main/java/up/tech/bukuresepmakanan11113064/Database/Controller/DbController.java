package up.tech.bukuresepmakanan11113064.Database.Controller;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Takiya on 12/04/2016.
 */

public class DbController extends SQLiteOpenHelper {
    private  static  final String DBname = "resepmakan.db";
    private  static  final  int DBVERSION =1;
    public DbController(Context context) {
        super(context, DBname, null, DBVERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
       db.execSQL(SaranController.CREATE_TABEL_SARAN);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(DBVERSION<2) {

        }
        else if (DBVERSION<3) {

        }
    }
}
