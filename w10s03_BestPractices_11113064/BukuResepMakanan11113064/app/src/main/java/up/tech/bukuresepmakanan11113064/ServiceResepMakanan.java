package up.tech.bukuresepmakanan11113064;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import java.util.Calendar;

/**
 * Created by Takiya on 01/03/2016.
 */


public class ServiceResepMakanan extends Service {
    private final static  String TAG = "Resep Service";
    private  boolean isRun = false;


    public boolean isRun() {
        return isRun;
    }

    public static String getTAG() {
        return TAG;
    }

    public void setIsRun(boolean isRun) {
        this.isRun = isRun;
    }

    @Override
    public void onCreate() {
        Log.i(TAG,"Resep Service OnCreate");


        setIsRun(true);

    }

    @Override
    public int onStartCommand (Intent intent ,  int flags ,  int startId) {

        Log.i(TAG, "Service on Start Command");
        Toast.makeText(this, "Service Started" , Toast.LENGTH_LONG).show();
        int hour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        int minute = Calendar.getInstance().get(Calendar.MINUTE);
        Toast.makeText(this, "Service Started " + hour+ " "+ minute , Toast.LENGTH_LONG).show();
        Log.i(TAG, "Service Time Now : " + hour);

        return  Service.START_STICKY;

     }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log. i(TAG, "Service onBind") ;
        return null;
    }

    @Override
    public void onDestroy() {
        isRun = false;
        Toast.makeText(this,"Service Destroyed", Toast.LENGTH_LONG).show();
        Log.i(TAG,"Service On Destroy");

    }
}
