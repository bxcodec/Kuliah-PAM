package up.tech.bukuresepmakanan11113064.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import up.tech.bukuresepmakanan11113064.R;

/**
 * Created by Takiya on 17/03/2016.
 */
public class GridAdapterMenu extends BaseAdapter {

//
//    int [] thumbImage;
//    String [] textImage;
       PacketData [] data;
    ArrayList<PacketData> dat;

    Context mcontext;
    public GridAdapterMenu(Context c, ArrayList<PacketData> dataSent) {
        mcontext=c;
        dat=dataSent;
    }

    @Override
    public int getCount() {
        return dat.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    class  ViewHolder {

        ImageView imageButton;
        TextView textView;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        LayoutInflater inflater = (LayoutInflater) mcontext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if(convertView==null) {
        convertView =  inflater.inflate(R.layout.grid_main_item,null);
            viewHolder = new ViewHolder();
         viewHolder.imageButton = (ImageView) convertView.findViewById(R.id.btnImageGrid);
         viewHolder.textView = (TextView) convertView.findViewById(R.id.lblBtnGrid);
        convertView.setTag(viewHolder);

        }
        else {
        viewHolder =(ViewHolder) convertView.getTag();
        }

        viewHolder.textView.setText(dat.get(position).getLbl());

//        Drawable d =mcontext.getResources().getDrawable(data [position].idData);
//        Drawable d = mcontext.getDrawable(dat.get(position).getIdData());
//        viewHolder.imageButton.setImageDrawable(d);
        Log.v("IMAN SEJAHTERA SELALAU" , dat.get(position).toString());
        viewHolder.imageButton.setImageDrawable(mcontext.getResources().getDrawable(dat.get(position).getIdData()));
//        viewHolder.imageButton.setImageResource(dat.get(position).getIdData());
        return convertView;
    }

//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        ImageButton imageView;
//        if(convertView==null) {
//
//            imageView =  new ImageButton(mcontext);
//            imageView.setLayoutParams(new GridView.LayoutParams(85,85));
//            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
//            imageView.setPadding(8, 8, 8, 8);
////            convertView.setTag(imageView);
//        }
//        else {
//            imageView = (ImageButton) convertView;
//        }
//
//        imageView.setImageResource(dat.get(position).getIdData());
//        Log.v("IMAN GITU LHO", "HEI SAYA");
//        return  imageView;
//    }
}
