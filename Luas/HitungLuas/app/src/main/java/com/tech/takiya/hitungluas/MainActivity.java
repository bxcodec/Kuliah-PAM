package com.tech.takiya.hitungluas;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {

    private EditText panjang;
    private  EditText lebar;
    private Button hitung;
    private TextView hasil;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Hitung Luas Persegi Panjang");

        panjang=(EditText) findViewById(R.id.panjang_input);
        lebar=(EditText) findViewById(R.id.lebar_input);
        hitung= (Button) findViewById(R.id.count);
        hasil = (TextView) findViewById(R.id.ResultView);

        hitung.setOnClickListener(  new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String P = panjang.getText().toString().trim();
                String L = lebar.getText().toString().trim();

                double pan = Double.parseDouble(P);
                double leb = Double.parseDouble(L);

                double luas = pan*leb;

                hasil.setText("Luas " + luas);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
